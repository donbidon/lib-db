<?php
/**
 * PDODigged class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Lib\DB;

use PDO;
use donbidon\Core\Event\Args;
use donbidon\Core\Event\Manager;

/**
 * PDODigged class unit tests.
 *
 * @todo Cover all functionality by tests.
 */
class PDODiggedTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * PDO instance
     *
     * @var PDODigged
     */
    protected $pdo;

    /**
     * Handled event
     *
     * @var array
     */
    protected $event;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $path = sprintf("%s/../tests.data", __DIR__);
        $registry = \donbidon\Core\Bootstrap::initByPath(sprintf("%s/config.php", $path));
        $logPath = sprintf("%s/db.log", $path);
        @unlink($logPath);
        $registry->set('core/db/log/path', $logPath);

        $this->pdo = new PDODigged(
            sprintf("sqlite:%s/sqlite.db", $path),
            "",
            "",
            [
                PDO::ATTR_EMULATE_PREPARES => FALSE,
                PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
            ]
        );
        $this->pdo->startLogging();
        $this->pdo->exec("DELETE FROM `test`");
        $this->pdo->exec("DELETE FROM `sqlite_sequence`");
        $this->pdo->exec("VACUUM");

        /**
         * @var Manager
         */
        $evtManager = $registry->get('core/event/manager');
        $evtManager->addHandler('db', [$this, 'hadleDBEvent']);
    }

    /**
     * Tetsts db/db debugging functionality.
     *
     * @return void
     * @covers donbidon\Lib\PDODigged::exec
     * @covers donbidon\Lib\PDODigged::query
     * @covers donbidon\Lib\PDOStatementDigged::__construct
     * @covers donbidon\Lib\PDOStatementDigged::fetch
     * @covers donbidon\Lib\T_Debug::debug
     */
    public function testDB()
    {
        $this->pdo->beginTransaction();

        $this->event = null;
        $actual = $this->pdo->exec(
            "INSERT INTO `test` " .
            "(`value1`, `value2`, `value3`) VALUES " .
            "(1, 2, 3)"
        );

        $this->assertTrue(is_array($this->event), "DB debug event not fired");
        $this->assertEquals(1, $actual);
        $this->assertEquals(4, $GLOBALS['_donbidon_']['benchmarks']['db']['queriesCount']);

        $this->event = null;
        $stmt = $this->pdo->query(
            "SELECT * " .
            "FROM `test`"
        );
        $actual = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->assertEquals([
            'id'     => 1,
            'value1' => 1,
            'value2' => 2,
            'value3' => 3,
        ], $actual);

        $this->pdo->rollBack();
    }

    /**
     * DB event handler.
     *
     * @param  string $name
     * @param  Args   $args
     * @return void
     */
    public function hadleDBEvent($name, Args $args)
    {
        $this->event = [
            'name' => $name,
            'args' => $args,
        ];
    }
}
