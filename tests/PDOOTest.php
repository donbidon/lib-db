<?php
/**
 * PDOO class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Lib\DB;

use PDO;
use PDOStatement;
use donbidon\Core\Event\Args;
use donbidon\Core\Event\Manager;

/**
 * PDOO class unit tests.
 */
class DPOOTest extends \donbidon\Lib\PHPUnit\TestCase
{
    /**
     * PDO instance
     *
     * @var PDO
     */
    protected $pdo;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $path = sprintf("%s/../tests.data", __DIR__);
        $registry = \donbidon\Core\Bootstrap::initByPath(sprintf("%s/config.php", $path));
        $logPath = sprintf("%s/db.log", $path);
        @unlink($logPath);
        $registry->set('core/db/log/path', $logPath);

        $this->pdo = new PDODigged(
            sprintf("sqlite:%s/sqlite.db", $path),
            "",
            "",
            [
                PDO::ATTR_EMULATE_PREPARES => FALSE,
                PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
            ]
        );
        $this->pdo->startLogging();
        $this->pdo->exec("DELETE FROM `test`");
        $this->pdo->exec("DELETE FROM `sqlite_sequence`");
        $this->pdo->exec("VACUUM");
    }

    /**
     * Tests PDOO::prepareModifyingStatement() method.
     *
     * @return void
     * @covers donbidon\Lib\DBPDOO::prepareModifyingStatement
     */
    public function testPrepareModifyingStatement()
    {
        $this->pdo->beginTransaction(); // {

        $template = "INSERT INTO `test` %s";
        $stmt = PDOO::prepareModifyingStatement(
            $this->pdo,
            $template,
            ['value1' => "1 + 1"]
        );
        $stmt->execute();
        $stmt = $this->pdo->query("SELECT * FROM `test` LIMIT 1");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->assertTrue(is_array($row), "No records fetched after insertion");
        $this->assertEquals("1 + 1", $row['value1']);

        $this->pdo->rollBack(); // }
        $this->pdo->beginTransaction(); // {

        $template = "INSERT INTO `test` %s";
        $stmt = PDOO::prepareModifyingStatement(
            $this->pdo,
            $template,
            [],
            [],
            ['value1' => "1 + 1"]
        );
        $stmt->execute();
        $stmt = $this->pdo->query("SELECT * FROM `test` LIMIT 1");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->assertTrue(is_array($row), "No records fetched after insertion");
        $this->assertEquals("2", $row['value1']);

        $this->pdo->rollBack(); // }
        $this->pdo->beginTransaction(); // {

        $template = "INSERT INTO `test` %s";
        $stmt = PDOO::prepareModifyingStatement(
            $this->pdo,
            $template,
            ['value1' => "text1", 'value3' => "5 * 5"],
            ['value3' => PDO::PARAM_INT],
            ['value2' => "4 * 4"]
        );
        $stmt->execute();
        $stmt = $this->pdo->query("SELECT * FROM `test` LIMIT 1");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->assertTrue(is_array($row), "No records fetched after insertion");
        $this->assertEquals("text1", $row['value1']);
        $this->assertEquals("16",    $row['value2']);
        $this->assertEquals("5",     $row['value3']);

        $template = "UPDATE `test` %s";
        $stmt = PDOO::prepareModifyingStatement(
            $this->pdo,
            $template,
            [],
            [],
            ['value1' => "2 * 2"]
        );
        $stmt->execute();
        $stmt = $this->pdo->query("SELECT * FROM `test` LIMIT 1");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->assertTrue(is_array($row), "No records fetched after insertion");
        $this->assertEquals(4, $row['value1']);

        $this->pdo->rollBack(); // }
    }
}
