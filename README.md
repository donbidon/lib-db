# Database related library

## Installing
Run ` composer --dev require donbidon/lib-db dev-master ` or add following code to your "composer.json" file:
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/lib-db"
        }
    ],
    "minimum-stability": "dev",
    "prefer-stable":      true,
    "require": {
        "donbidon/lib-db": "dev-master"
    }
```
and run ` composer update `.

## So what's inside?
Consider the situation when a record coming to some method of DAL class can has different set of fields, but every time method has to store time, when record was saved. In this case we cannot use prepared statement, so we have to generate SQL-template every time record is coming. PDOO::prepareModifyingStatement() decides this problem.
```php
class DAL
{
    /**
     * @var PDO
     */
    protected $pdo;
    // ...
    public function saveRecord($id, array $record, array $types)
    {
        // ...
        $stmt = \donbidon\Lib\DB\PDOO::prepareModifyingStatement(
            $this->pdo,
            "UPDATE `table` %s WHERE id = :id",
            $record,
            $types, // fields types, see PDO::PARAM_...
            ['time_updated' => "NOW()"]
        );
        $stmt->bindValue(':id' , $id);
        $stmt->execute();
        // ...
    }
    // ...
}
```
