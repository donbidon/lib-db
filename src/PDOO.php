<?php
/**
 * PDO related library.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\DB;

use PDO;
use PDOStatement;

/**
 * PDO related library.
 *
 * @static
 */
class PDOO
{
    /**
     * Prepares modifying statement, binds values with raw values support.
     * Useful when you don't know, which fields will be updated and static
     * SQL-template couldn't be used.
     *
     * Example:
     * ```php
     * $template = "UPDATE `table` %s WHERE `id` = :id";
     * $stmt = PDOO::prepareModifyingStatement(
     *     $pdo,
     *     $template,
     *     ['field' => "value"], // once time this fields, other time other fields
     *     [],
     *     ['time_updated' => "NOW()"]
     * );
     * $stmt->bindValue(':id', 100500, PDO::PARAM_INT);
     * $stmt->execute();
     * ```
     * SQ- template that will be generated for PDO::prepare() inside method:
     * ```sql
     * UPDATE `table` SET `field` = :field, `time_updated` = NOW() WHERE `id` = :id
     * ```
     *
     * @param  PDO    $pdo        PDO instance
     * @param  string $template   SQL query template i. e.
     *         ```UPDATE `table` %s WHERE `id` = :id```
     * @param  array  $values     Common values
     * @param  array  $types      Common values data types
     * @param  array  $rawValues  Raw values
     * @return PDOStatement
     */
    public static function prepareModifyingStatement(
        PDO $pdo,
        $template,
        array $values,
        array $types = [],
        array $rawValues = []
    )
    {
        $fields = array_map(
            function ($field)
            {
                return sprintf("`%s`", $field);
            },
            array_merge(array_keys($values), array_keys($rawValues))
        );
        $placeholders = array_merge(
            array_map(
                function ($field)
                {
                    return sprintf(":%s", $field);
                },
                array_keys($values)
            ),
            array_values($rawValues)
        );
        if (preg_match("/^\s?insert\s+/si", $template)) {
            $query = sprintf(
                $template,
                sprintf(
                    "(%s) VALUES (%s)",
                    implode(", ", $fields),
                    implode(", ", $placeholders)
                )
            );
        } else {
            $set = [];
            foreach ($fields as $index => $field) {
                $set[] = sprintf("%s = %s", $field, $placeholders[$index]);
            }
            $query = sprintf(
                $template,
                sprintf("SET %s", implode(", ", $set))
            );
        }

        $stmt = $pdo->prepare($query);
        foreach ($values as $placeholder => $value) {
            isset($types[$placeholder])
                ? $stmt->bindValue(sprintf(":%s", $placeholder), $value, $types[$placeholder])
                : $stmt->bindValue(sprintf(":%s", $placeholder), $value);
        }

        return $stmt;
    }
}
