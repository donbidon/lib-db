<?php
/**
 * PDOStatement having debugging abilities class.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\DB;

use PDO;
use PDOStatement;

/**
 * PDO having debugging abilities class.
 *
 * @see http://php.net/manual/en/pdostatement.php
 */
class PDOStatementDigged implements \Iterator
{
    use T_Debug;
    use \donbidon\Core\Log\T_Logger;

    /**
     * SQL query template
     *
     * @var string
     */
    protected $template;

    /**
     * PDO object
     *
     * @var PDO
     */
    protected $pdo;

    /**
     * "Parent" statement object
     *
     * @var PDOStatement
     */
    protected $stmt;

    /**
     * Binded values
     *
     * @var array
     */
    protected $values = [];

    /**
     * Constructor.
     *
     * @param string       $template
     * @param PDO          $pdo
     * @param PDOStatement $stmt
     */
    public function __construct($template, PDO $pdo, PDOStatement $stmt)
    {
        $this->template = $template;
        $this->pdo      = $pdo;
        $this->stmt     = $stmt;

        if ($this->isCorePresent()) {
            $this->evtManager = \donbidon\Core\Registry\Recursive::_get('core/event/manager');
        }
    }

    /**
     * Some kind of magic.
     *
     * @param  string $name
     * @param  array  $args
     * @return mixed
     */
    public function __call($name, array $args)
    {
        $this->fireDebuggingEvents();
        $t = microtime(TRUE);

        $result = call_user_func_array([$this->stmt, $name], $args);

        if ("fetch" == substr($name, 0, 5)) {
            ++$GLOBALS['_donbidon_']['benchmarks']['db']['fetchingCount'];
            $GLOBALS['_donbidon_']['benchmarks']['db']['fetchingTime'] +=
                microtime(TRUE) - $t;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function current()
    {
        $result = $this->stmt->current();

        return $reult;
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function key()
    {
        $result = $this->stmt->key();

        return $reult;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function next()
    {
        $this->stmt->next();
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function rewind()
    {
        $this->stmt->rewind();
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function valid()
    {
        $result = $this->stmt->valid();

        return $reult;
    }

    /**
     * {@inheritdoc}
     *
     * @param  mixed $parameter
     * @param  mixed $value
     * @param  int   $dataType
     * @return bool
     */
    public function bindValue($parameter, $value, $dataType = PDO::PARAM_STR)
    {
        $this->fireDebuggingEvents();
        $this->values[$parameter] = [$value, $dataType];

        $result = $this->stmt->bindValue($parameter, $value, $dataType);
        // trigger_error("{$parameter} " . var_export($value, TRUE));###

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  array $inputParameters
     * @return bool
     */
    public function execute(array $inputParameters = [])
    {
        $this->fireDebuggingEvents();
        foreach ($inputParameters as $parameter => $value) {
            $this->bindValue($parameter, $value);
        }
        $this->render();
        $t = microtime(TRUE);

        $result = $this->stmt->execute();

        ++$GLOBALS['_donbidon_']['benchmarks']['db']['queriesCount'];
        $GLOBALS['_donbidon_']['benchmarks']['db']['queriesTime'] +=
            microtime(TRUE) - $t;

        return $result;
    }

    /**
     * Replaces placeholders and fires debug event.
     *
     * @return void
     */
    protected function render()
    {
        $query = $this->template;
        if (sizeof($this->values) > 0) {
            $marker = sprintf("-%s-", md5(microtime(TRUE)));
            $query = str_replace("?", $marker, $query);
            $search = sprintf("/%s/", preg_quote($marker));
            foreach ($this->values as $field => $data) {
                $dataType = $data[1] & ~PDO::PARAM_INPUT_OUTPUT;
                switch ($dataType) {
                    case PDO::PARAM_BOOL:
                        $value = (int)(bool)$data[0];
                        break;
                    case PDO::PARAM_NULL:
                        $value = "null";
                        break;
                    case PDO::PARAM_INT:
                        $value = (int)$data[0];
                        break;
                    default:
                        $value = $this->pdo->quote($data[0], $data[1]);
                        break;
                }
                $query = is_string($field)
                    ? str_replace(
                        is_string($field) ? $field : '?',
                        $value,
                        $query
                    )
                    : preg_replace($search, $value);
            }
            $query = str_replace($marker, "?", $query);
        }
        $this->fireDebuggingEvents([
            'source' => sprintf("%s()", __METHOD__),
            'args'   => [0 => $query]
        ]);
        if ($this->isCorePresent()) {
            $this->log($query, 'db:query');
        }
    }
}
