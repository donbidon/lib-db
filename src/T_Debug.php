<?php
/**
 * Database debugging trait.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\DB;

use donbidon\Core\Registry\Recursive;
use donbidon\Core\Event\Args;
use donbidon\Core\Event\Manager;

/**
 * Database debugging trait.
 */
trait T_Debug
{
    /**
     * Flag specifying is logging started
     *
     * @var bool
     */
    protected $isLoggingStarted = FALSE;

    /**
     * Starts final queries logging.
     * Expects 'core/db/log' section in registry, i.e. section in config:
     * ```ini
     * [core]
     * ; ...
     * db.log[method]   = "File" ; "StdOut" now supported too.
     * db.log[level]    = E_NOTICE
     * db.log.source[] = "db:query"
     *
     * ; db.log[path]     = "/path/to/db/queries/log" ; will be setup from test, for file logger, see donbidon\Lib\FileSystem\Logger
     * ; db.log[maxSize]  = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
     * ; db.log[rotation] = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
     * ; db.log[righs]    = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
     * ```
     * Usage:
     * ```php
     * <?php
     *
     * require_once "/path/to/vendor/autoload.php";
     * // ...
     * \donbidon\Core\Bootstrap::initByPath("/path/to/config");
     *
     * $pdo = new \donbidon\Lib\DB\PDODigged(...);
     * $pdo->startQueriesLogging();
     * ```
     * Ater calling of this method queries with replaced placeholders will\
     * be logget to file.
     *
     * @return void
     */
    public function startLogging()
    {
        if (!$this->isCorePresent()) {
            return;
        }

        $registry = new Recursive([
            'core' => [
                'log' => Recursive::_get('core/db/log'),
            ],
        ]);
        \donbidon\Core\Log\Factory::run(
            $registry,
            $this->evtManager
        );
        $this->isLoggingStarted = TRUE;
    }

    /**
     * Returns true if core is present.
     *
     * @return bool
     */
    protected function isCorePresent()
    {
        $result = class_exists("\\donbidon\\Core\\Event\\Manager", FALSE);

        return $result;
    }

    /**
     * Fires database debugging events.
     *
     * @param    array $args
     * @return   void
     * @internal
     */
    protected function fireDebuggingEvents($args = null)
    {
        if (!$this->isCorePresent()) {
            return;
        }

        if (is_null($args)) {
            $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
            $source = sprintf("%s::%s()", $trace[1]['class'], $trace[1]['function']);
            $args = new Args([
                'source' => $source,
                'args'   => $trace[1]['args'],
            ]);
        } else {
            $source = $args['source'];
            $args = new Args($args);
        }
        /**
         * @var Manager
         */
        $evtManager = Recursive::_get('core/event/manager');
        $evtManager->fire('db', $args);
        $evtManager->fire(sprintf("db:%s", $source), $args);
    }
}
