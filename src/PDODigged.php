<?php
/**
 * PDO having debugging abilities class.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\DB;

use PDO;

require_once sprintf("%s/init.php", __DIR__);

/**
 * PDO having debugging abilities class.
 *
 * @see http://php.net/manual/en/pdo.php
 */
class PDODigged extends PDO
{
    use T_Debug;
    use \donbidon\Core\Log\T_Logger;

    /**
     * {@inheritdoc}
     *
     * @param string $dsn
     * @param string $username
     * @param string $passwd
     * @param array  $options
     */
    public function __construct(
        $dsn,
        $username = "",
        $passwd = "",
        array $options = []
    )
    {
        parent::__construct($dsn, $username, $passwd, $options);

        if ($this->isCorePresent()) {
            $this->evtManager = \donbidon\Core\Registry\Recursive::_get('core/event/manager');
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function beginTransaction()
    {
        $this->fireDebuggingEvents();

        $result = parent::beginTransaction();

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function commit()
    {
        $this->fireDebuggingEvents();
        $t = microtime(TRUE);

        $result = parent::commit();

        ++$GLOBALS['_donbidon_']['benchmarks']['db']['commitsCount'];
        $GLOBALS['_donbidon_']['benchmarks']['db']['commitsTime'] +=
            microtime(TRUE) - $t;

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $statement
     * @return int
     */
    public function exec($statement)
    {
        $this->fireDebuggingEvents();
        if ($this->isCorePresent()) {
            $this->log($statement, 'db:query');
        }
        $t = microtime(TRUE);

        $result = parent::exec($statement);

        ++$GLOBALS['_donbidon_']['benchmarks']['db']['queriesCount'];
        $GLOBALS['_donbidon_']['benchmarks']['db']['queriesTime'] +=
            microtime(TRUE) - $t;

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $statement
     * @return PDOStatementDigged
     */
    public function query($statement)
    {
        $this->fireDebuggingEvents();
        if ($this->isCorePresent()) {
            $this->log($statement, 'db:query');
        }
        $t = microtime(TRUE);

        $stmt = parent::query($statement);

        ++$GLOBALS['_donbidon_']['benchmarks']['db']['queriesCount'];
        $GLOBALS['_donbidon_']['benchmarks']['db']['queriesTime'] +=
            microtime(TRUE) - $t;
        $result = new PDOStatementDigged($statement, $this, $stmt);
        if ($this->isLoggingStarted) {
            $result->startLogging();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $statement
     * @param  array  $driverOptions
     * @return PDOStatementDigged
     */
    public function prepare($statement, $driverOptions = [])
    {
        $this->fireDebuggingEvents();
        $t = microtime(TRUE);

        $stmt = parent::prepare($statement, $driverOptions);

        ++$GLOBALS['_donbidon_']['benchmarks']['db']['preparedQueriesCount'];
        $GLOBALS['_donbidon_']['benchmarks']['db']['preparedQueriesTime'] +=
            microtime(TRUE) - $t;
        $result = new PDOStatementDigged($statement, $this, $stmt);
        if ($this->isLoggingStarted) {
            $result->startLogging();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function rollBack()
    {
        $this->fireDebuggingEvents();

        $result = parent::rollBack();

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @param  int   $attribute
     * @param  mixed $value
     * @return bool
     */
    public function setAttribute($attribute, $value)
    {
        $this->fireDebuggingEvents();

        $result = parent::setAttribute($attribute, $value);

        return $result;
    }
}
