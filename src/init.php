<?php
/**
 * Initialization.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

if (!isset($GLOBALS['_donbidon_'])) {
    $GLOBALS['_donbidon_'] = [];
}
if (!isset($GLOBALS['_donbidon_']['benchmarks'])) {
    $GLOBALS['_donbidon_']['benchmarks'] = [];
}
if (!isset($GLOBALS['_donbidon_']['benchmarks']['db'])) {
    $GLOBALS['_donbidon_']['benchmarks']['db'] = [
        'queriesCount'         => 0,
        'queriesTime'          => 0,
        'preparedQueriesCount' => 0,
        'preparedQueriesTime'  => 0,
        'fetchingCount'        => 0,
        'fetchingTime'         => 0,
        'commitsCount'         => 0,
        'commitsTime'          => 0,
    ];
}
