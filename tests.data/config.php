;<?php die; __halt_compiler();
[core]

; By default: Off
;event[debug] = On

; By default: "StdOut"
;log[method] = "UTLogMethod"

; By default: E_ERROR | E_WARNING
log[level] = E_ERROR | E_WARNING | E_NOTICE

;log.source[] = "*" ; means all log sources
;log.source[] = "LogTest::someMethod()"

db.log[method]   = "File" ; "StdOut" now supported too.
db.log.level    = E_NOTICE
db.log.source[] = "db:query"

; db.log[path ]    = "/path/to/db/queries/log" ; will be setup from test, for file logger, see donbidon\Lib\FileSystem\Logger
; db.log[maxSize]  = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; db.log[rotation] = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
; db.log[righs]    = ...   ; For file logger, see donbidon\Lib\FileSystem\Logger
